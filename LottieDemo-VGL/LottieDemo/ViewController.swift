//  ViewController.swift
//  Created by Valente Guevara Lopez on 28/8/2019.
//  Copyright © 2019 Guevara Dev. All rights reserved.
//

import UIKit
import Lottie

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let animationView = LOTAnimationView(name: "433-checked-done") {
            //Asignar dimenciones a la View
            animationView.frame = CGRect(x: 0, y: 0, width: 400, height: 400)
            animationView.center = self.view.center
           //Esta línea repite la animación para que siempre se anime.
            animationView.loopAnimation = true
            
            animationView.contentMode = .scaleAspectFill
            animationView.animationSpeed = 0.5
            
            // Applying UIView animation
            let minimizeTransform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            animationView.transform = minimizeTransform
            UIView.animate(withDuration: 3.0, delay: 0.0, options: [.repeat, .autoreverse], animations: {
                animationView.transform = CGAffineTransform.identity
            }, completion: nil)
            
            view.addSubview(animationView)
            
            //Esta función básicamente reproduce la animación conectada.
            animationView.play()
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

